package com.dbshac2hire.thankcoin.external.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 *
 * @author isak.rabin
 */
@SpringBootApplication
@PropertySources(
        @PropertySource("classpath:application.properties"))
public class ThanksCoinExternalAPI {

    public static void main(String[] args) {
        SpringApplication.run(ThanksCoinExternalAPI.class, args);
    }

}
