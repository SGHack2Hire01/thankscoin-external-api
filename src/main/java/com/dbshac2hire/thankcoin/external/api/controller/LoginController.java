package com.dbshac2hire.thankcoin.external.api.controller;

import com.dbshac2hire.thankcoin.external.api.dto.LoginDto;
import com.dbshac2hire.thankcoin.external.api.service.LoginService;
import com.dbshac2hire.thankcoin.external.api.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isak.rabin
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<LoginVo> login(@RequestBody LoginDto login) {
        LoginVo result = loginService.login(login.getEmail(), login.getPassword());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
