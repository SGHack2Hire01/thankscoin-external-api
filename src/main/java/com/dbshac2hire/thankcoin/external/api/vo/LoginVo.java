package com.dbshac2hire.thankcoin.external.api.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author isak.rabin
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginVo extends BaseVo {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
