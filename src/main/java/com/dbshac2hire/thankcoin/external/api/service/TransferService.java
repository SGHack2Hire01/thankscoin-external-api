package com.dbshac2hire.thankcoin.external.api.service;

import com.dbshac2hire.thankcoin.external.api.dto.TransferDto;
import com.dbshac2hire.thankcoin.external.api.vo.TransferResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author isak.rabin
 */
@Service
public class TransferService {

    private final String LEDGER_ENDPOINT = "http://localhost:8080/";
    private final String WALLET_ENDPOINT = "http://localhost:8090";

    @Autowired
    private RestClientService restClient;

    public TransferResultVo send(String token, String fromAccount, String toEmail, Long amount, String description, String signature) {

        String transferURL = WALLET_ENDPOINT + "/accounts/" + fromAccount + "/transfer";

        TransferDto transferDto = new TransferDto();
        transferDto.setAccountFrom(fromAccount);
        transferDto.setEmailTo(toEmail);
        transferDto.setAmount(amount);
        transferDto.setDescription(description);
        transferDto.setSignature(signature);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        
        HttpEntity request = new HttpEntity(transferDto, headers);
        
        ResponseEntity<TransferResultVo> result = restClient.getRestTemplate().exchange(transferURL, HttpMethod.POST, request, TransferResultVo.class);
        //ResponseEntity<TransferResultVo> result = restClient.getRestTemplate().postForEntity(transferURL, transferDto, TransferResultVo.class);
        return result.getBody();
    }

    public TransferResultVo receieve(String fromAccount, String toAccount, Long amount, String description, String signature) {
        TransferDto transaction = new TransferDto();
        transaction.setAccountFrom(fromAccount);
        transaction.setAccountTo(toAccount);
        transaction.setAmount(amount);
        transaction.setDescription(description);
        transaction.setSignature(signature);

        String transferAccountURL = LEDGER_ENDPOINT + "/transaction/";
        restClient.getRestTemplate().postForEntity(transferAccountURL, transaction, String.class);

        String retrieveAccountURL = LEDGER_ENDPOINT + "/account/" + fromAccount;
        return restClient.getRestTemplate().getForObject(retrieveAccountURL, TransferResultVo.class);
    }
}
