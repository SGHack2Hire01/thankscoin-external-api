package com.dbshac2hire.thankcoin.external.api.dto;

/**
 *
 * @author isak.rabin
 */
public class LoginDto {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
